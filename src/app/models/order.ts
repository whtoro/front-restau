export class Order{
	constructor(
		public id: number,
		public id_user: number,
		public mesa_id: number,
		public createdAt: any,
		public updatedAt: any
	){}
}