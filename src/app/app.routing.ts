import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Componentes
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DefaultComponent } from './components/default/default.component';
import { OrderNewComponent } from './components/order-new/order-new.component';
import { OrderEditComponent } from './components/order-edit/order-edit.component';
import { OrderDetailComponent } from './components/order-detail/order-detail.component';

const appRoutes: Routes = [
	{path:'', component: DefaultComponent},
	{path:'inicio', component: DefaultComponent},
	{path:'login', component: LoginComponent},
	{path:'logout/:sure', component: LoginComponent},
	{path:'registro', component: RegisterComponent},
	{path:'crear-orden', component: OrderNewComponent},
	{path:'editar-coche/:id', component: OrderEditComponent},
	{path:'coche/:id', component: OrderDetailComponent},
	{path:'**', component: DefaultComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);