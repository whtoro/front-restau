import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';
import {Order} from '../models/order';

@Injectable()
export class OrderService {
	public url: string;

	constructor(
		public _http: HttpClient
	){
		this.url = GLOBAL.url;
	}

	pruebas(){
		return "Hola mundo!!";
	}

	create(token, order: Order): Observable<any>{
		let json = JSON.stringify(order);
		let params = "json="+json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
									   .set('Authorization', token);

		return this._http.post(this.url + 'orders', params, {headers: headers});							   
	}

	getOrders(): Observable<any>{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
		return this._http.get(this.url + 'orders', {headers: headers});
	}

	getOrder(id): Observable<any>{
		return this._http.get(this.url + 'orders/' + id);
	}

	update(token, order, id): Observable<any>{
		let json = JSON.stringify(order);
		let params = "json="+json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
									   .set('Authorization', token);

		return this._http.put(this.url + 'orders/' + id, params, {headers: headers});
	}

	delete(token, id): Observable<any>{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
									   .set('Authorization', token);

		return this._http.delete(this.url + 'orders/' + id, {headers: headers});
	}

}