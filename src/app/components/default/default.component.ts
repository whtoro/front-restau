import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service'; 
import { Order } from '../../models/order';
import { OrderService } from '../../services/order.service'; 

@Component({
	selector: 'default',
	templateUrl: './default.component.html',
	providers: [UserService, OrderService]
})
export class DefaultComponent implements OnInit{
	public title: string;
	public orders: Array<Order>;
	public token;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _orderService: OrderService
	){
		this.title = 'Inicio';
		this.token = this._userService.getToken();
	}

	ngOnInit(){
		console.log('default.component ordergado correctamente!!');
		this.getOrders();
	}

	getOrders(){
		this._orderService.getOrders().subscribe(
			response => {
				if(response.status == 'success'){
					this.orders = response.orders;
				}
			},
			error => {
				console.log(error);
			}
		);
	}

	deleteOrder(id){
		this._orderService.delete(this.token, id).subscribe(
			response => {
				// this._router.navigate(['/home']);
				this.getOrders();
			},
			error => {
				console.log(<any>error);
			}
		);
	}

}