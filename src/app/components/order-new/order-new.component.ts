import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Order } from '../../models/order';
import { OrderService } from '../../services/order.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-order-new',
  templateUrl: './order-new.component.html',
  styleUrls: ['./order-new.component.css'],
  providers: [UserService, OrderService]
})
export class OrderNewComponent implements OnInit {
	public page_title: string;
	public identity;
	public token;
	public order: Order;
	public status_order:string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _orderService: OrderService
	){ 
		this.page_title = 'Crear nueva Orden';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
	}

	ngOnInit() {
		if(this.identity == null){
			this._router.navigate(["/login"]);
		}else{
			// Crear objeto cochce
			this.order = new Order(1, 1, 1, null, null);
		}
	}

	onSubmit(form){
		this._orderService.create(this.token, this.order).subscribe(
			response => {
				
				if(response.status == 'success'){
					this.order = response.order;
					this.status_order = 'success';
					this._router.navigate(['/home']);
				}else{
					this.status_order = 'error';
				}
				
			},	
			error => {
				console.log(<any>error);
				this.status_order = 'error';
			}
		);
	}

}
