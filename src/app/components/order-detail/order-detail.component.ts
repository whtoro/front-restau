import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service'; 
import { Order } from '../../models/order';
import { OrderService } from '../../services/order.service'; 

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  providers: [UserService, OrderService]
})
export class OrderDetailComponent implements OnInit {
  public order: Order;

  constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _orderService: OrderService
	){
		
	}

  ngOnInit() {
  	this.getOrder();
  }

  getOrder(){
  	this._route.params.subscribe(params => {
  		let id = +params['id'];

  		this._orderService.getOrder(id).subscribe(
  			response => {
  				if(response.status == 'success'){
  					this.order = response.order;
  				}else{
  					this._router.navigate(['home']);
  				}
  			},
  			error => {
  				console.log(<any>error);
  			}
  		);
  	});
  }

}
