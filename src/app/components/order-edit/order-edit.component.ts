import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service'; 
import { Order } from '../../models/order';
import { OrderService } from '../../services/order.service'; 

@Component({
  selector: 'app-order-edit',
  templateUrl: '../order-new/order-new.component.html',
  providers: [UserService, OrderService]
})
export class OrderEditComponent implements OnInit {
  public page_title: string;
  public order: Order;
  public token;
  public status_order;

  constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _orderService: OrderService
	){
		this.token = this._userService.getToken();
	}

  ngOnInit() {
  	this._route.params.subscribe(params => {
  		let id = +params['id'];
  		this.getOrder(id);
	});
  }

  getOrder(id){
  		this._orderService.getOrder(id).subscribe(
  			response => {
  				if(response.status == 'success'){
  					this.order = response.order;
  					//this.page_title = 'Editar ' + this.order.title;
  				}else{
  					this._router.navigate(['home']);
  				}
  			},
  			error => {
  				console.log(<any>error);
  			}
  		);
  }

  onSubmit(form){
  	console.log(this.order.id);
  	this._orderService.update(this.token, this.order, this.order.id).subscribe(
  		response => {
  			if(response.status == 'success'){
  				this.status_order = 'success';
  				this.order = response.order;
  				this._router.navigate(['/coche', this.order.id]);
  			}else{
  				this.status_order = 'error';
  			}

		},
		error => {
			console.log(<any>error);
			this.status_order = 'error';
		}
  	);
  }

}
